def read_data(file_name: str):
    for line in open(file_name):
        parts = line.strip().split()
        names = parts[0]
        marks = parts[1:]